// @ts-ignore
import Alpine from 'alpinejs';
import { Direction, Swiper } from './swiper';
import './index.css';

type BottomModal = 'legend' | 'profile' | 'summary';

// constants
const locale = localStorage.getItem('locale') ?? 'ru';
const date = new Date();
const today = date.getDate();

const monthYearFormatter = new Intl.DateTimeFormat(locale, {
    month: 'long',
    year: 'numeric',
});
const weekdayFormatter = new Intl.DateTimeFormat(locale, { weekday: 'short' });

const currentPeriod = monthYearFormatter.format(date);
const startOfWeek = new Date(
    new Date().setDate(today - (date.getDay() || 7) + (locale === 'ru' ? 1 : 0))
);
const weekdays = [...new Array(7)]
    .map((_, i) => i)
    .map((v) => {
        const initialDate = new Date(startOfWeek);
        const weekDay = initialDate.setDate(initialDate.getDate() + v);
        return weekdayFormatter.format(weekDay);
    });

const currentPeriodParts = currentPeriod.split(' ');
currentPeriodParts.pop();

currentPeriodParts[0] = `${currentPeriodParts[0][0].toUpperCase()}${currentPeriodParts[0].slice(
    1
)}`;

/* Stores */

// common
Alpine.store('app', {
    isViewModePrimary: true,
    isStatsActive: false,
    hasNotifications: true,
    activeView: null as string | null,
    activeModal: null as BottomModal | null,
    swiper: null as Swiper | null,
    secondaryViews: {
        notifications: 'Уведомления',
        tags: 'Теги',
        recognition: 'Биометрия',
        debug: 'Отладка'
    },

    headerStyles: [
        'app-header',
        'bg-primary',
        'color-white',
        'd-flex',
        'flex-row',
        'flex-center',
        'shadow-1',
        'pad-h-5',
    ].join(' '),

    contentStyles: ['app-content', 'pad-v-4'].join(' '),

    viewModeToggle() {
        this.isViewModePrimary = !this.isViewModePrimary;
    },

    setSecondaryView(view: string) {
        let delay = 0;
        if (this.activeModal) {
            this.setModal(null);
            delay = 0.3 * 1000;
        }

        setTimeout(() => {
            this.activeView = view;
            this.isViewModePrimary = false;
        }, delay);
    },

    setModal(modal: BottomModal | null) {

        if (modal) {
            if (modal === 'profile') this.isStatsActive = false;

            this.activeModal = modal;
            setTimeout(() => {
                const $modal = document.querySelector('[data-modal] > .popup-dialog-bottom');

                if ($modal) {
                    $modal?.classList.add('popup-initialized');
                    const handler = (direction: Direction) => {
                        if (direction === Direction.DOWN) this.setModal(null);
                    };
                    const options = { delta: 'ontouchstart' in document.documentElement ? 5 : 50 };
                    this.swiper = new Swiper($modal as HTMLElement, handler, options);
                }
            }, 0);
        }
        else {
            const $modal = document.querySelector('[data-modal] > .popup-dialog-bottom');
            $modal?.classList.remove('popup-initialized');

            if (this.swiper) {
                this.swiper.destroy();
                this.swiper = null;
            }

            setTimeout(() => {
                this.activeModal = null;
            }, 0.2 * 1000)
        }

    },

    tryToDeactivateModal(e: Event) {
        const $target = e.target as HTMLElement;
        if ($target.hasAttribute('data-modal')) this.setModal(null);
    },

    updateLocale(locale: string) {
        localStorage.setItem('locale', locale);
        setTimeout(() => window.location.replace(window.location.href), 0);
    },
});

// calendar
Alpine.store('calendar', {
    header: currentPeriodParts.join(' '),
    weekdays,
    scrollBarWidth: 0,
    isWeeksContainerScrolled: false,
    isScheduleView: false,
    selectedDay: null as number | null,

    onWeeksInit() {
        const $el: HTMLElement = document.querySelector(
            '.app-calendar-weeks'
        ) as HTMLElement;
        if ($el) {
            $el.addEventListener('scroll', () => {
                this.isWeeksContainerScrolled = Boolean($el.scrollTop)
            });

            // todo: destroy
            new Swiper($el, (direction) => {
                if ([Direction.RIGHT, Direction.LEFT].includes(direction)) {
                    console.log(direction === Direction.RIGHT ? 'Previous month' : 'Next month')
                }
            });

            const detectScrollBar = () => {
                setTimeout(() => {
                    const scrollBarWidth = $el.offsetWidth - $el.clientWidth;
                    this.scrollBarWidth = scrollBarWidth;
                }, 0);
            };
            detectScrollBar();

            new ResizeObserver(detectScrollBar).observe($el);
        }
    },

    selectDay(day: number) {
        this.selectedDay = day;
        Alpine.store('app').setModal('summary')
    },

    scrollToToday() {
        setTimeout(() => {
            const $today = document.querySelector('.app-calendar-today');
            if ($today) {
                const $parent = $today.closest('.scroll-y') as Element;
                const diff = $today.getBoundingClientRect().top - $parent.getBoundingClientRect().top;
                $parent.scrollTop += diff - 16;
            }
        }, 0)
    }
});

// secondary views
Alpine.store('secondary', {
    //
});

// init
Alpine.start();
