import { Swiper } from './swiper';

// todo
export class Dragger extends Swiper {

    init = () => {
        this.el.addEventListener('pointerdown', this.handleStart)
        this.el.addEventListener('pointermove', this.handleMove)
    }

    destroy = () => {
        this.el.removeEventListener('pointerdown', this.handleStart)
        this.el.addEventListener('pointermove', this.handleMove)
    }

    handleMove = (e: PointerEvent) => {
        const { clientX, clientY } = e;
    }
}
