export enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
}

interface SwiperOptions {
    delay?: number; // difference between starting and finishing gestures
    delta?: number; // minimum space between staring and finishing points
}

export class Swiper {
    el: HTMLElement;
    handler: (direction: Direction) => void;

    private delay: number;
    private delta: number;

    private isActive: Boolean = false;
    private startedAt!: number;

    private startX!: number;
    private startY!: number;

    private endX!: number;
    private endY!: number;

    constructor(
        element: HTMLElement,
        handler: (direction: Direction) => void,
        options: SwiperOptions = {}
    ) {
        this.el = element;
        this.handler = handler;

        const { delay, delta } = options;
        this.delay = delay ?? 350;
        this.delta = delta ?? 150;

        this.handleStart = this.handleStart.bind(this);
        this.handleMove = this.handleMove.bind(this);
        this.handleEnd = this.handleEnd.bind(this);

        this.init();
    }

    init = () => {
        this.el.addEventListener('pointerdown', this.handleStart, false);
        this.el.addEventListener('pointermove', this.handleMove, false);
        this.el.addEventListener('pointerup', this.handleEnd, false);
        this.el.addEventListener('pointerleave', this.handleEnd, false);
        this.el.addEventListener('pointercancel', this.handleEnd, false);
    };

    destroy = () => {
        this.el.removeEventListener('pointerdown', this.handleStart);
        this.el.removeEventListener('pointermove', this.handleMove);
        this.el.removeEventListener('pointerup', this.handleEnd);
        this.el.removeEventListener('pointerleave', this.handleEnd);
        this.el.removeEventListener('pointercancel', this.handleEnd);
    };

    handleStart = (e: PointerEvent) => {
        this.isActive = true;
        e.preventDefault();

        const { clientX: startX, clientY: startY } = e;

        this.startedAt = Date.now();
        this.startX = startX;
        this.startY = startY;
    };

    handleMove = (e: PointerEvent) => {
        e.preventDefault();
        if (e.pointerType !== 'touch') return;

        const { clientX: endX, clientY: endY } = e;
        this.endX = endX;
        this.endY = endY;
    };

    handleEnd = (e: PointerEvent) => {
        e.preventDefault();
        // incative
        if (!this.isActive) return;
        this.isActive = false;

        const diffTime = Date.now() - this.startedAt;
        
        // too long
        if (diffTime > this.delay) return;

        if (e.pointerType !== 'touch') {
            const { clientX: endX, clientY: endY } = e;
            this.endX = endX;
            this.endY = endY;
        }

        const diffX = this.endX - this.startX;
        const diffY = this.endY - this.startY;

        const diffXAbs = Math.abs(diffX);
        const diffYAbs = Math.abs(diffY);
        const isVerticalDirection = diffXAbs < diffYAbs;

        // too small delta
        if ((isVerticalDirection ? diffYAbs : diffXAbs) < this.delta) return;

        const directionDetected = isVerticalDirection
            ? diffY > 0
                ? Direction.DOWN
                : Direction.UP
            : diffX > 0
                ? Direction.RIGHT
                : Direction.LEFT;

        this.handler(directionDetected);
    };
}
