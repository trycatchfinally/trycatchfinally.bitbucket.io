const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const config = {
    // Change to your "entry-point".
    entry: './src/index',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js',
        publicPath: '/dist/',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
    },
    // devtool: 'inline-source-map',
    plugins: [],
    module: {
        rules: [
            {
                // Include ts, tsx, js, and jsx files.
                test: /\.(ts|js)x?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
};

module.exports = (_env, argv) => {
    if (argv.mode === 'production') {
        config.plugins = [new MiniCssExtractPlugin()];
        config.module.rules[1] = {
            test: /\.css$/i,
            use: [MiniCssExtractPlugin.loader, 'css-loader'],
        };
    }
    return config;
};
